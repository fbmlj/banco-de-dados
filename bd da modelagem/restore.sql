--
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.leciona DROP CONSTRAINT leciona_fk_id_professor_fkey;
ALTER TABLE ONLY public.leciona DROP CONSTRAINT leciona_fk_id_disciplina_fkey;
ALTER TABLE ONLY public.leciona DROP CONSTRAINT leciona_fk_id_aula_fkey;
ALTER TABLE ONLY public.aula DROP CONSTRAINT aula_fk_id_sala_fkey;
ALTER TABLE ONLY public.aluno DROP CONSTRAINT aluno_fk_id_turma_fkey;
ALTER TABLE ONLY public.aluno DROP CONSTRAINT aluno_fk_id_endereco_fkey;
ALTER TABLE ONLY public.turma DROP CONSTRAINT turma_pkey;
ALTER TABLE ONLY public.turma DROP CONSTRAINT turma_codigo_turma_key;
ALTER TABLE ONLY public.sala DROP CONSTRAINT sala_pkey;
ALTER TABLE ONLY public.professor DROP CONSTRAINT professor_pkey;
ALTER TABLE ONLY public.professor DROP CONSTRAINT professor_matricula_key;
ALTER TABLE ONLY public.professor DROP CONSTRAINT professor_cpf_key;
ALTER TABLE ONLY public.leciona DROP CONSTRAINT leciona_pkey;
ALTER TABLE ONLY public.endereco DROP CONSTRAINT endereco_pkey;
ALTER TABLE ONLY public.disciplina DROP CONSTRAINT disciplina_pkey;
ALTER TABLE ONLY public.aula DROP CONSTRAINT aula_pkey;
ALTER TABLE ONLY public.aluno DROP CONSTRAINT aluno_pkey;
ALTER TABLE ONLY public.aluno DROP CONSTRAINT aluno_matricula_key;
ALTER TABLE ONLY public.aluno DROP CONSTRAINT aluno_fk_id_endereco_key;
ALTER TABLE ONLY public.aluno DROP CONSTRAINT aluno_cpf_key;
ALTER TABLE public.turma ALTER COLUMN id_turma DROP DEFAULT;
ALTER TABLE public.sala ALTER COLUMN id_sala DROP DEFAULT;
ALTER TABLE public.professor ALTER COLUMN id_professor DROP DEFAULT;
ALTER TABLE public.leciona ALTER COLUMN id_leciona DROP DEFAULT;
ALTER TABLE public.endereco ALTER COLUMN id_endereco DROP DEFAULT;
ALTER TABLE public.disciplina ALTER COLUMN id_disciplina DROP DEFAULT;
ALTER TABLE public.aula ALTER COLUMN id_aula DROP DEFAULT;
ALTER TABLE public.aluno ALTER COLUMN id_aluno DROP DEFAULT;
DROP SEQUENCE public.turma_id_turma_seq;
DROP TABLE public.turma;
DROP SEQUENCE public.sala_id_sala_seq;
DROP TABLE public.sala;
DROP SEQUENCE public.professor_id_professor_seq;
DROP TABLE public.professor;
DROP SEQUENCE public.leciona_id_leciona_seq;
DROP TABLE public.leciona;
DROP SEQUENCE public.endereco_id_endereco_seq;
DROP TABLE public.endereco;
DROP SEQUENCE public.disciplina_id_disciplina_seq;
DROP TABLE public.disciplina;
DROP SEQUENCE public.aula_id_aula_seq;
DROP TABLE public.aula;
DROP SEQUENCE public.aluno_id_aluno_seq;
DROP TABLE public.aluno;
DROP EXTENSION plpgsql;
DROP SCHEMA tentando;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: tentando; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tentando;


ALTER SCHEMA tentando OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aluno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aluno (
    id_aluno integer NOT NULL,
    cpf character(11),
    cr numeric,
    nome character varying(50) NOT NULL,
    matricula character(5) NOT NULL,
    fk_id_turma integer,
    fk_id_endereco integer NOT NULL
);


ALTER TABLE public.aluno OWNER TO postgres;

--
-- Name: aluno_id_aluno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aluno_id_aluno_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aluno_id_aluno_seq OWNER TO postgres;

--
-- Name: aluno_id_aluno_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aluno_id_aluno_seq OWNED BY public.aluno.id_aluno;


--
-- Name: aula; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aula (
    id_aula integer NOT NULL,
    horario character(10) NOT NULL,
    fk_id_sala integer NOT NULL
);


ALTER TABLE public.aula OWNER TO postgres;

--
-- Name: aula_id_aula_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aula_id_aula_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aula_id_aula_seq OWNER TO postgres;

--
-- Name: aula_id_aula_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aula_id_aula_seq OWNED BY public.aula.id_aula;


--
-- Name: disciplina; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.disciplina (
    id_disciplina integer NOT NULL,
    nome character(15) NOT NULL,
    ementa character(30)
);


ALTER TABLE public.disciplina OWNER TO postgres;

--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.disciplina_id_disciplina_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disciplina_id_disciplina_seq OWNER TO postgres;

--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.disciplina_id_disciplina_seq OWNED BY public.disciplina.id_disciplina;


--
-- Name: endereco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.endereco (
    id_endereco integer NOT NULL,
    rua character(30) NOT NULL,
    cep integer NOT NULL,
    numero integer NOT NULL,
    complemento character(30),
    cidade character(20) NOT NULL
);


ALTER TABLE public.endereco OWNER TO postgres;

--
-- Name: endereco_id_endereco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.endereco_id_endereco_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.endereco_id_endereco_seq OWNER TO postgres;

--
-- Name: endereco_id_endereco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.endereco_id_endereco_seq OWNED BY public.endereco.id_endereco;


--
-- Name: leciona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.leciona (
    id_leciona integer NOT NULL,
    fk_id_aula integer NOT NULL,
    fk_id_professor integer NOT NULL,
    fk_id_disciplina integer NOT NULL
);


ALTER TABLE public.leciona OWNER TO postgres;

--
-- Name: leciona_id_leciona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.leciona_id_leciona_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.leciona_id_leciona_seq OWNER TO postgres;

--
-- Name: leciona_id_leciona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.leciona_id_leciona_seq OWNED BY public.leciona.id_leciona;


--
-- Name: professor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.professor (
    id_professor integer NOT NULL,
    nome character(50) NOT NULL,
    matricula character(5) NOT NULL,
    cpf character(11) NOT NULL
);


ALTER TABLE public.professor OWNER TO postgres;

--
-- Name: professor_id_professor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.professor_id_professor_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professor_id_professor_seq OWNER TO postgres;

--
-- Name: professor_id_professor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.professor_id_professor_seq OWNED BY public.professor.id_professor;


--
-- Name: sala; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sala (
    id_sala integer NOT NULL,
    capacidade integer NOT NULL,
    andar character(5) NOT NULL,
    complemento character(20),
    numero integer
);


ALTER TABLE public.sala OWNER TO postgres;

--
-- Name: sala_id_sala_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sala_id_sala_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sala_id_sala_seq OWNER TO postgres;

--
-- Name: sala_id_sala_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sala_id_sala_seq OWNED BY public.sala.id_sala;


--
-- Name: turma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.turma (
    id_turma integer NOT NULL,
    semestre integer NOT NULL,
    codigo_turma character(5) NOT NULL,
    serie character varying(20) NOT NULL
);


ALTER TABLE public.turma OWNER TO postgres;

--
-- Name: turma_id_turma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.turma_id_turma_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turma_id_turma_seq OWNER TO postgres;

--
-- Name: turma_id_turma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.turma_id_turma_seq OWNED BY public.turma.id_turma;


--
-- Name: aluno id_aluno; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno ALTER COLUMN id_aluno SET DEFAULT nextval('public.aluno_id_aluno_seq'::regclass);


--
-- Name: aula id_aula; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aula ALTER COLUMN id_aula SET DEFAULT nextval('public.aula_id_aula_seq'::regclass);


--
-- Name: disciplina id_disciplina; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.disciplina ALTER COLUMN id_disciplina SET DEFAULT nextval('public.disciplina_id_disciplina_seq'::regclass);


--
-- Name: endereco id_endereco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco ALTER COLUMN id_endereco SET DEFAULT nextval('public.endereco_id_endereco_seq'::regclass);


--
-- Name: leciona id_leciona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leciona ALTER COLUMN id_leciona SET DEFAULT nextval('public.leciona_id_leciona_seq'::regclass);


--
-- Name: professor id_professor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor ALTER COLUMN id_professor SET DEFAULT nextval('public.professor_id_professor_seq'::regclass);


--
-- Name: sala id_sala; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sala ALTER COLUMN id_sala SET DEFAULT nextval('public.sala_id_sala_seq'::regclass);


--
-- Name: turma id_turma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma ALTER COLUMN id_turma SET DEFAULT nextval('public.turma_id_turma_seq'::regclass);


--
-- Data for Name: aluno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aluno (id_aluno, cpf, cr, nome, matricula, fk_id_turma, fk_id_endereco) FROM stdin;
\.
COPY public.aluno (id_aluno, cpf, cr, nome, matricula, fk_id_turma, fk_id_endereco) FROM '$$PATH$$/2998.dat';

--
-- Data for Name: aula; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aula (id_aula, horario, fk_id_sala) FROM stdin;
\.
COPY public.aula (id_aula, horario, fk_id_sala) FROM '$$PATH$$/3002.dat';

--
-- Data for Name: disciplina; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.disciplina (id_disciplina, nome, ementa) FROM stdin;
\.
COPY public.disciplina (id_disciplina, nome, ementa) FROM '$$PATH$$/3006.dat';

--
-- Data for Name: endereco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.endereco (id_endereco, rua, cep, numero, complemento, cidade) FROM stdin;
\.
COPY public.endereco (id_endereco, rua, cep, numero, complemento, cidade) FROM '$$PATH$$/3010.dat';

--
-- Data for Name: leciona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.leciona (id_leciona, fk_id_aula, fk_id_professor, fk_id_disciplina) FROM stdin;
\.
COPY public.leciona (id_leciona, fk_id_aula, fk_id_professor, fk_id_disciplina) FROM '$$PATH$$/3008.dat';

--
-- Data for Name: professor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.professor (id_professor, nome, matricula, cpf) FROM stdin;
\.
COPY public.professor (id_professor, nome, matricula, cpf) FROM '$$PATH$$/3004.dat';

--
-- Data for Name: sala; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sala (id_sala, capacidade, andar, complemento, numero) FROM stdin;
\.
COPY public.sala (id_sala, capacidade, andar, complemento, numero) FROM '$$PATH$$/3000.dat';

--
-- Data for Name: turma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.turma (id_turma, semestre, codigo_turma, serie) FROM stdin;
\.
COPY public.turma (id_turma, semestre, codigo_turma, serie) FROM '$$PATH$$/2996.dat';

--
-- Name: aluno_id_aluno_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aluno_id_aluno_seq', 8, true);


--
-- Name: aula_id_aula_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aula_id_aula_seq', 2, true);


--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.disciplina_id_disciplina_seq', 3, true);


--
-- Name: endereco_id_endereco_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.endereco_id_endereco_seq', 1, true);


--
-- Name: leciona_id_leciona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.leciona_id_leciona_seq', 4, true);


--
-- Name: professor_id_professor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.professor_id_professor_seq', 2, true);


--
-- Name: sala_id_sala_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sala_id_sala_seq', 2, true);


--
-- Name: turma_id_turma_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.turma_id_turma_seq', 7, true);


--
-- Name: aluno aluno_cpf_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_cpf_key UNIQUE (cpf);


--
-- Name: aluno aluno_fk_id_endereco_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_fk_id_endereco_key UNIQUE (fk_id_endereco);


--
-- Name: aluno aluno_matricula_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_matricula_key UNIQUE (matricula);


--
-- Name: aluno aluno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_pkey PRIMARY KEY (id_aluno);


--
-- Name: aula aula_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aula
    ADD CONSTRAINT aula_pkey PRIMARY KEY (id_aula);


--
-- Name: disciplina disciplina_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.disciplina
    ADD CONSTRAINT disciplina_pkey PRIMARY KEY (id_disciplina);


--
-- Name: endereco endereco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT endereco_pkey PRIMARY KEY (id_endereco);


--
-- Name: leciona leciona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leciona
    ADD CONSTRAINT leciona_pkey PRIMARY KEY (id_leciona);


--
-- Name: professor professor_cpf_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_cpf_key UNIQUE (cpf);


--
-- Name: professor professor_matricula_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_matricula_key UNIQUE (matricula);


--
-- Name: professor professor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_pkey PRIMARY KEY (id_professor);


--
-- Name: sala sala_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sala
    ADD CONSTRAINT sala_pkey PRIMARY KEY (id_sala);


--
-- Name: turma turma_codigo_turma_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma
    ADD CONSTRAINT turma_codigo_turma_key UNIQUE (codigo_turma);


--
-- Name: turma turma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma
    ADD CONSTRAINT turma_pkey PRIMARY KEY (id_turma);


--
-- Name: aluno aluno_fk_id_endereco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_fk_id_endereco_fkey FOREIGN KEY (fk_id_endereco) REFERENCES public.endereco(id_endereco);


--
-- Name: aluno aluno_fk_id_turma_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_fk_id_turma_fkey FOREIGN KEY (fk_id_turma) REFERENCES public.turma(id_turma);


--
-- Name: aula aula_fk_id_sala_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aula
    ADD CONSTRAINT aula_fk_id_sala_fkey FOREIGN KEY (fk_id_sala) REFERENCES public.sala(id_sala);


--
-- Name: leciona leciona_fk_id_aula_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leciona
    ADD CONSTRAINT leciona_fk_id_aula_fkey FOREIGN KEY (fk_id_aula) REFERENCES public.aula(id_aula);


--
-- Name: leciona leciona_fk_id_disciplina_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leciona
    ADD CONSTRAINT leciona_fk_id_disciplina_fkey FOREIGN KEY (fk_id_disciplina) REFERENCES public.disciplina(id_disciplina);


--
-- Name: leciona leciona_fk_id_professor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leciona
    ADD CONSTRAINT leciona_fk_id_professor_fkey FOREIGN KEY (fk_id_professor) REFERENCES public.professor(id_professor);


--
-- PostgreSQL database dump complete
--

